const net = require('net');
const models = require('./models.js');

const port = process.env.SERVER_PORT;

const server = net.Server();

server.listen(port, () => {
    console.log('Simple TCP server started');
});

server.on('connection', socket => {
    console.log(`Received connection from ${socket.remoteAddress}:${socket.remotePort}`);

    socket.on('data', data => {
        console.log(`Received data: ${data}`);
        console.log(`Text: ${data.toString()}`);
        models.store_data(socket.remoteAddress, socket.remotePort, data, data.toString());
    });

    socket.on('end', () => {
        console.log('Remote end closed connection');
    });

    socket.on('error', error => {
        console.log(error);
    });
})
