const {Sequelize, DataTypes} = require('sequelize');

let connection = `${process.env.DIALECT}://`;
connection+= `${process.env.USERNAME}:`;
connection += `${process.env.PASSWORD}@`;
connection += `${process.env.HOST}:`;
connection += `${process.env.DB_PORT}/`;
connection += `${process.env.DB_NAME}`;

const sequelize = new Sequelize(connection, {logging: false});

const SimpleData = sequelize.define('simple_data', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true
    },
    remote_address: {
        type: DataTypes.STRING,
        allowNull: false
    },
    remote_port: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    raw_payload: {
        type: DataTypes.STRING.BINARY,
        allowNull: false
    },
    text_payload: {
        type: DataTypes.TEXT,
        allowNull: false
    }
});

async function store_data(address, port, raw, text){
    const data = SimpleData.build({
        remote_address: address,
        remote_port: port,
        raw_payload: raw,
        text_payload: text
    });

    try{
        const result = await data.save();
        console.log('Stored', result.dataValues);
    }
    catch(error){
        console.error(error.original);
    }
}

module.exports = {store_data}
