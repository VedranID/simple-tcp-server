FROM node:12.16.3

RUN useradd --create-home simple_tcp

USER simple_tcp

WORKDIR /home/simple_tcp

COPY --chown=simple_tcp package.json ./

RUN npm i

COPY --chown=simple_tcp . ./

CMD node src/simple_tcp.js
