# Simple TCP server

A simple TCP server which logs all received data to help us determine data format the client is sending us.

## Running

Set environment variables. For example

```
DIALECT: postgres
USERNAME: postgres
PASSWORD: secretpassword
HOST: db
DB_PORT: 5432
DB_NAME: postgres
SERVER_PORT: 8080
PORT_IN: 8080
PORT_OUT: 8080
```

* DIALECT is the type of database used
* USERNAME is the database username
* PASSWORD is the database password
* HOST is the database IP address
* DB_PORT is the port where database is listening
* DB_NAME specifies which database to connect to
* SERVER_PORT simple TCP should listen on this port
* PORT_IN same as server_port
* PORT_OUT same as PORT_IN, for port mapping

```bash
docker-compose up
```

## Send test data

```bash
echo -n 'aloha' | nc localhost $PORT_OUT
```

## Dependencies

Simple TCP server depends on Postgresql which must contain the following table:

```
CREATE TABLE simple_data
(
  id SERIAL PRIMARY KEY,
  remote_address VARCHAR NOT NULL,
  remote_port INTEGER NOT NULL,
  raw_payload bytea NOT NULL,
  text_payload TEXT NOT NULL,
  "createdAt" timestamp with time zone DEFAULT now(),
  "updatedAt" timestamp with time zone DEFAULT now()
);

```
