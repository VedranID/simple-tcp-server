BEGIN;
CREATE EXTENSION IF NOT EXISTS hstore;

CREATE TABLE simple_data
(
  id SERIAL PRIMARY KEY,
  remote_address VARCHAR NOT NULL,
  remote_port INTEGER NOT NULL,
  raw_payload bytea NOT NULL,
  text_payload TEXT NOT NULL,
  "createdAt" timestamp with time zone DEFAULT now(),
  "updatedAt" timestamp with time zone DEFAULT now()
);

COMMIT;
